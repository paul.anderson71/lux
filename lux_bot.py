#!/usr/bin/python3

# This IRC bot keeps track of the ship and responds to commands.
import socket
import time
import json

SOCKNUM = 6667
BOTNAME = "lux"
WARPSPEED = 1000 # TODO
IMPULSESPEED = 10 # TODO

GAMEFILE = "game_data.json"
STATEFILE = "state_data.json"

class Connection:
    def __init__(self, host):
        self.sock = socket.socket()
        self.sock.connect((host, SOCKNUM))
        self.sock.send(bytes(
            f"USER {BOTNAME} {BOTNAME} {BOTNAME} {BOTNAME}\r\n", "UTF-8"))
        self.sock.send(bytes(
            f"NICK {BOTNAME}\r\n", "UTF-8"))
        return
    def _send(self, raw_msg):
        self.sock.send(bytes(f"{raw_msg}\r\n", "UTF-8"))
        return
    def pong(self):
        # respond to server ping.
        self._send("PONG")
        return
    def join(self, channel):
        self._send(f"JOIN #{channel}")
        return
    def privmsg(self, target, msg):
        # send a message to the target
        self._send(f("PRIVMSG {target} :{msg}"))
        return

def distance_squared(p1, p2):
    return sum(p1[i]*p2[i] for i in range(3))

def vector_towards(p1, p2, magnitude):
    # Returns a vector pointing from p1 to p2 with the given magnitude
    base = [p2[i]-p1[i] for i in range(3)]
    size = distance_squared(base, [0, 0, 0])**0.5
    return [x*magnitude/size for x in base]

def star_at_pos(game_data, pos):
    # Return None if there's no star here
    for star, location in game_data["starmap"].items():
        if location == pos:
            return star
    return None

def announce_event(connection, game_data, event_class, msg):
    for room, tags in game_data["rooms"].items():
        if event_class == "all" or event_class in tags:
            connection.privmsg(f"#{room}", msg)
    return

def move_ship_start(state_data):
    changed = False
    output = []
    if not state_data["docked"]:
        if state_data["warp"]:
            vel = WARPSPEED = state_data["warp"]
            if state_data["target"]:
                if distance_squared(state_data["pos"], state_data["target"]) < (vel * vel):
                    # Getting close ... drop out of warp
                    changed = True
                    state_data["warp"] = 0
                    state_data["vel"] = IMPULSESPEED
                    output.append(("navigation", "Approaching destination. Slowing to impulse."))
            else:
                # Target disappeared?
                changed = True
                state_data["warp"] = 0
                state_data["vel"] = IMPULSESPEED
                output.append(("navigation", "Nothing to warp to. Slowing to impulse."))
        if not state_data["warp"]:
            vel = state_data["vel"]
            if state_data["target"]:
                if distance_squared(state_data["pos"], state_data["target"]) < (vel * vel):
                    # We're here!
                    changed = True
                    state_data["vel"] = 0
                    state_data["pos"] = state_data["target"]
                    # Target is set to None in step function, not here
            else:
                # Target disappeared?
                # Continuing with impulse is fine. We can always warp back.
                pass
    return (changed, output)

def move_ship_end(state_data)
    # Move the ship.
    if not state_data["docked"] and (state_data["warp"] or state_data["vel"]):
        changed = True
        if state_data["warp"]:
            vel = WARPSPEED * state_data["warp"]
        else:
            vel = float(state_data["vel"])
        vec = vector_towards(state_data["pos"], state_data["target"], vel)
        state_data["pos"] = [state_data["pos"][i] + vec[i] for i in range(3)]
    return

def load_game_data():
    # Data format:
    # {
    #   "rooms":{"roomname":[allowed_actions]},
    #   "starmap":{"starname":[x,y,z]}
    # }
    try:
        with open(GAMEFILE, "r") as f:
            data = json.load(f)
    except FileNotFoundError:
        data = None
    return data

def load_state_data():
    try:
        with open(STATEFILE, "r") as f:
            data = json.load(f)
    except FileNotFoundError:
        data = None
    return data

def save_state_data(state_data):
    with open(STATEFILE, "w") as f:
        json.dump(state_data, f, indent=4)
    return

def step(connection, game_data, state_data):
    change_state = False
    # Update (per second).
    # = Start of second =
    # TODO Check whether the server has sent us anything.
    msg_queue = []

    # TODO Respond to server as appropriate (e.g. with a PONG)

    # TODO Process player commands, such as "!course Sol", "!sensors",
    #      possibly giving players feedback

    # To check logs, ask IRC server for actual logs

    for msg in msg_queue:
        if not msg.startswith("!"):
            continue

    # Move the ship.
    changed, output = move_ship_start(state_data) # Updates state_data
    if changed:
        change_state = True
    if output:
        for tag, msg in output:
            announce_event(connection, game_data, tag, msg)

    # Checking for docking happens here, not in move_ship_start, because it requires game_data
    if state_data["pos"] == state_data["target"]:
        state_data["target"] = None
        state_data["docked"] = star_at_pos(game_data, state_data["pos"])
        announce_event(connection, game_data, "navigation", "Arrived at destination.")

    # = End of second =
    changed, output = move_ship_end(state_data) # Updates state_data
    if changed:
        change_state = True
    if change_state:
        save_state(state_data)

    return

def main():
    connection = Connection("localhost")
    game_data = load_game_data()
    state_data = load_state_data()
    if not state_data:
        state_data = {
            "pos":      [0, 0, 0],      # x, y, z
            "vel":      0,              # impulse
            "warp":     0,
            "docked":   "sol",
            "target":   None,
            "player_pos":{}
        }
    while True:
        t = time.time()
        step(connection, game_data, state_data)
        time.sleep(1 - (time.time() - t))

if __name__=="__main__":
    main()
